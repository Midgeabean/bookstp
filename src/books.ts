import express from 'express'
import {v4} from 'uuid'
import bodyParser from 'body-parser'
import { user, usersList } from './user';

export const books_router = express.Router()

type Book = {
    uuid: string,
    nom: string,
    pages: number,
    owner: user
}

let bookList: Book[] = []

books_router.use(bodyParser.json())
books_router.use((req, res, next) => {
    const cookies = req.headers.cookie?.split("; ")
    req.cookies = {}
    cookies?.forEach((cookie) => {
        const cookieData = cookie.split("=")
        req.cookies[cookieData[0]] = cookieData[1]
    })
    next()
})
books_router.use((req, res, next) => {
    const userUUid = req.cookies["authentication"]
    const user = usersList.get(userUUid)
    
    res.locals.user = user

    next()
})


books_router.get('/', (req, res) => {
    let userBook: Book[] = bookList.filter(book => book.owner.uuid == res.locals.user.uuid)
    res.send(userBook)
})

books_router.post('/', (req, res) => {
    if(res.locals.user != null) {
        let book: Book = req.body
    
        book.uuid = v4()
        book.owner = res.locals.user

        bookList.push(book)
        res.status(201)
    
        res.send(book)
    } else {
        res.send("BAH LAPINOU t'es pas connecté dis")
    }
})

books_router.get('/:id', (req, res) => {
    let id = req.params['id']

    let book: Book | undefined = bookList.find(book => book.uuid == id && book.owner.uuid == res.locals.user.uuid)

    if(book) {
        res.send(book)
    } else {
        res.send("j'ai pas ca dans ma biblio")
    }
})

books_router.put('/:id', (req, res) => {
    let id = req.params['id']
    let data: Book = req.body

    let book: Book | undefined = bookList.find(book => book.uuid == id && book.owner.uuid == res.locals.user.uuid)

    if(book) {
        book.nom = data.nom
        book.pages = data.pages
        res.send(book)
    } else {
        res.send("j'ai pas ca dans ma biblio")
    }
})

books_router.delete('/:id', (req, res) => {
    let id = req.params['id']

    bookList = bookList.filter(book => book.uuid != id && book.owner.uuid == res.locals.user.uuid)

    res.send("c'est fait")
})