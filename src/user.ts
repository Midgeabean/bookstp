import express from 'express'
import {v4} from 'uuid'
import bodyParser from 'body-parser'

export const user_router = express.Router()

export type user = {
    firstName: string,
    lastName: string,
    email: string,
    password: string,
    uuid: string
} 

export const usersList: Map<string, user> = new Map<string,user>()

user_router.use(bodyParser.json())
user_router.use((req, res, next) => {
    const cookies = req.headers.cookie?.split("; ")
    req.cookies = {}
    cookies?.forEach((cookie) => {
        const cookieData = cookie.split("=")
        req.cookies[cookieData[0]] = cookieData[1]
    })
    next()
})

user_router.post('/register', (req, res) => {
       const data: user = req.body
        data.uuid = v4()
    
        let emailexist : boolean = false 
        usersList.forEach(currentUser => {
            if(currentUser.email == req.body.email){
                emailexist = true
            }
        });
        
        if(!emailexist){
        usersList.set(data.uuid, data)
        
        res.status(201)
        
        res.send(data)
        }
        else {
            res.send("Bah alors mon lapin tu veux une carotte ?")
        }
})


user_router.post('/login', (req, res) => {
    let localUser: user | null = null
    
    usersList.forEach(currentUser => {
        if(currentUser.email == req.body.email){
            localUser = currentUser
        }
    });

    if(localUser !== null){
        // @ts-ignore
        if(localUser.password == req.body.password){
            res.cookie(
                'authentication',
                // @ts-ignore
                localUser.uuid,
                {
                    maxAge: 36000
                }
            )
            res.json({message: "connected"})
        }

    }
    res.send()
})


user_router.post('/logout', (req, res) => {
    res.cookie("authentication",null,{
        maxAge: -1
    })
    res.send()

})


